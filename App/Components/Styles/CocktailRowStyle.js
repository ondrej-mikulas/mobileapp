import { StyleSheet } from 'react-native'
import { Metrics, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    paddingLeft: 5,
    paddingTop: 5
  },
  image: {
    width: ((Metrics.screenWidth - 5) / 2) - 5,
    height: ((Metrics.screenHeight - 5) / 4) - 5,
    justifyContent: 'flex-end'
  },
  labelContainer: {
    backgroundColor: Colors.blackTransparent,
    height: Metrics.section,
    justifyContent: 'center'
  },
  label: {
    color: Colors.white,
    marginLeft: Metrics.smallMargin
  }
})
